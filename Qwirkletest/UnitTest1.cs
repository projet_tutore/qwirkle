﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using QwirkleClass;

namespace QwirkleTest
{
    [TestClass]
    public class Joueurtest // classe qui teste les methodes de la classe Joueur
    {
        [TestMethod]
        public void GetNomTest()
        {
            Joueur test = new Joueur("Jean", 1997, 4, false);

            Assert.AreEqual("Jean", test.GetNom());
        }
        [TestMethod]
        public void GetBirthDateTest()
        {
            Joueur test = new Joueur("Jean", 1997, 4, false);

            Assert.AreEqual(1997, test.GetBirthdate());
        }
        [TestMethod]
        public void GetFirstPlayerTest()
        {
            Joueur test = new Joueur("Jean", 1997, 4, true);

            Assert.AreEqual(true, test.GetFirstPlayer());
        }
        [TestMethod]
        public void GetCombMaxTest()
        {
            Joueur test = new Joueur("Jean", 1997, 4, false);

            Assert.AreEqual(4, test.GetCombMax());
        }
        [TestMethod]
        public void CalculCombMaxTest()
        {
            Joueur test1 = new Joueur("j1", 1999, 5, false);
            Joueur test2 = new Joueur("j2", 1994, 4, false);
            Joueur test3 = new Joueur("j3", 1997, 3, false);

            List<Joueur> players = new List<Joueur>();
            players.Add(test1); players.Add(test2); players.Add(test3);

            Assert.AreEqual(5, test1.CalculCombMax(players));
        }
        [TestMethod]
        public void PremierJoueurTest()
        {
            Joueur test1 = new Joueur("j1", 1999, 5, false);
            Joueur test2 = new Joueur("j2", 1994, 4, false);
            Joueur test3 = new Joueur("j3", 1997, 3, false);

            List<Joueur> players = new List<Joueur>();
            players.Add(test1); players.Add(test2); players.Add(test3);

            Assert.AreEqual("j1 est le joueur qui commencera la partie", test1.PremierJoueur(players));
        }
    }
}
