﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Tuile 
    {
        internal string couleurT, formeT;
        internal int ID_Tuile;
        internal static List<Tuile> Pioche = new List<Tuile>();

        public Tuile(string couleurT, string formeT, int ID_Tuile) // constructeur de la classe tuile
        {
            this.couleurT = couleurT;
            this.formeT = formeT;
            this.ID_Tuile = ID_Tuile;
        }
        public void CreerPioche() // methode servant a creer la pioche
        {
            Tuile tuilep = new Tuile("", "", 0);

            for (int nbtuile = 1; nbtuile <= 3; nbtuile++) // il doit y avoir 3 exemplaires de chaque tuile
            {
                for (int forme = 0; forme <= 5; forme++) // il doit y avoir 6 formes differentes
                {
                    for (int couleur = 0; couleur <= 5; couleur++) // il doit y avoir 6 couleurs differentes
                    {
                        if (forme == 0)
                        {
                            tuilep.formeT = "trefle";
                        }

                        if (forme == 1)
                        {
                            tuilep.formeT = "cercle";
                        }

                        if (forme == 2)
                        {
                            tuilep.formeT = "losange";
                        }

                        if (forme == 3)
                        {
                            tuilep.formeT = "carre";
                        }

                        if (forme == 4)
                        {
                            tuilep.formeT = "etoile";
                        }

                        if (forme == 5)
                        {
                            tuilep.formeT = "croix";
                        }


                        if (couleur == 0)
                        {
                            tuilep.couleurT = "rouge";
                        }

                        if (couleur == 1)
                        {
                            tuilep.couleurT = "orange";
                        }

                        if (couleur == 2)
                        {
                            tuilep.couleurT = "violet";
                        }

                        if (couleur == 3)
                        {
                            tuilep.couleurT = "jaune";
                        }

                        if (couleur == 4)
                        {
                            tuilep.couleurT = "bleu";
                        }

                        if (couleur == 5)
                        {
                            tuilep.couleurT = "vert";
                        }
                        Pioche.Add(tuilep);
                        tuilep.ID_Tuile += 1;
                    }
                }
            }
        }


    }
}