﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0060:Supprimer le paramètre inutilisé", Justification = "<En attente>", Scope = "member", Target = "~M:QwirkleClass.Joueur.CalculScore(System.Int32,System.String,System.String,System.Int32,System.Int32,System.Int32)~System.Int32")]

