﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ClassLibrary1
{
    public class Joueur
    {
        internal string nom;
        internal int birthdate;
        internal int combinaison_max = 1;
        internal bool first_player;
        internal List<Joueur> players = new List<Joueur>();
        internal List<Tuile> Main = new List<Tuile>();

        public Joueur(string nom, int birthdate, int combinaison_max, bool first_player) // constructeur de la classe joueur
        {
            this.nom = nom;
            this.birthdate = birthdate;
            this.first_player = first_player;
            this.combinaison_max = combinaison_max;
        }
        public string GetNom()
        {
            return this.nom;
        }
        public int GetBirthdate()
        {
            return this.birthdate;
        }
        public int GetCombMax()
        {
            return this.combinaison_max;
        }
        public bool GetFirstPlayer()
        {
            return this.first_player;
        }
        public void SetNom(string nom)
        {
            this.nom = nom;
        }
        public void SetBirthdate(int birthdate)
        {
            this.birthdate = birthdate;
        }
        public void SetCombMax(int combinaison_max)
        {
            this.combinaison_max = combinaison_max;
        }
        public void SetFirstPlayer(bool first_player)
        {
            this.first_player = first_player;
        }
        public int CalculCombMax(List<Joueur> players)/* méthode pour calculer la plus grande combinaison dont dispose chaque joueur*/
        {
            int nb_joueurs; nb_joueurs = players.Count;
            int nb_tuiles_main;

            int rang_combmax_player = 0;
            Random rang_joueur = new Random();

            foreach (Joueur j in players)
            {
                nb_tuiles_main = Main.Count;

                foreach (Tuile t in Main)
                {
                    if (t.couleurT != Main.ElementAt(Main.IndexOf(t) + 1).couleurT || t.formeT != Main.ElementAt(Main.IndexOf(t) + 1).formeT) /* on compare la forme et la couleur des tuiles 2 à 2*/
                    {
                        j.combinaison_max += 1;
                    }
                }
                if (j.combinaison_max > players.ElementAt(rang_joueur.Next(nb_joueurs)).combinaison_max) /* ensuite si un joueur a une plus grande combinaison que les autres joueurs on sort de la boucle apres avoir memorise son rang dans la liste à travers la variable rang_combmax_player*/
                {
                    rang_combmax_player = players.IndexOf(j);
                    break;
                }
            }
            return players.ElementAt(rang_combmax_player).combinaison_max;
        }
        public string PremierJoueur(List<Joueur> players) // methode determinant quel joueur joue en premier grace a son nombre de coups maximal
        {
            int nb_joueurs; nb_joueurs = players.Count;
            int comb_sup = 0;
            int rang_first_player = 0;

            foreach (Joueur j in players)
            {
                for (int i = 0; i < nb_joueurs; i++) /* i étant le compteur permettant de parcourir la liste des joueurs*/
                {
                    if (j.combinaison_max > players.ElementAt(i).combinaison_max) /*on compare la plus grande combinaison que chaque joueur a*/
                    {
                        comb_sup += 1;
                    }
                }
                if (comb_sup == nb_joueurs - 1) /* si un des joueurs a une plus grande combinaison que les autres joueurs, il validera alors cette condition ce qui nous permettra de passer à true son attribut first_player ainsi que de memoriser son rang dans la liste */
                {
                    rang_first_player = players.IndexOf(j);
                    j.first_player = true;
                    break;
                }
            }
            return String.Format("{0} est le joueur qui commencera la partie", players.ElementAt(rang_first_player).nom);
        }
        public void Piocher() // methode permettant aux joueurs de piocher
        {
            int nb_tuiles_min;
            int nb_tuiles_main;

            nb_tuiles_main = Main.Count;
            int rang_tuile_pioche;
            Random rang_tuile = new Random();

            for (nb_tuiles_min = 0; nb_tuiles_min < 6 - nb_tuiles_main; nb_tuiles_min++) /* on effectue un certain nombre d'iterations selon le nombre de tules qu'un joueur a dans sa main*/
            {
                Tuile t = new Tuile("", "", 0);

                rang_tuile_pioche = rang_tuile.Next(Tuile.Pioche.Count);         /* on mémorise tout d'abord le rang de la tuile qui va être piochée */
                t = Tuile.Pioche.ElementAt(rang_tuile.Next(Tuile.Pioche.Count)); /* on stocke dans une nouvelle tuile les attributs d'une tuile choisie aléatoirement dans la pioche*/
                Main.Add(t);                                                     /* on ajoute ensuite la nouvelle tuile dans la main du joueur*/
                Tuile.Pioche.Remove(Tuile.Pioche.ElementAt(rang_tuile_pioche)); /* puis on supprime la tuile de la pioche*/
            }

        }
        public int CalculScore(int score, string couleurT, string formeT, int nbtuiles, int nbtuilescol, int nbtuileslig) // methode servant a calculer le score des joueurs
        {
            int nvpoints = 0;
            if (nbtuiles == 0)
            {
                nvpoints = 1; // si la tuile est la premiere a etre placee de la partie alors il n'y en a forcement pas d'autre dans la ligne ou la colonne donc elle ne rapporte qu'un point
            }

            else // pour chaque tuile presente dans la ligne on ajoute un point et de meme pour la colonne
            {
                if (nbtuilescol == 0)
                {
                    if (nbtuileslig == 1)
                        nvpoints = 2;

                    if (nbtuileslig == 2)
                        nvpoints = 3;

                    if (nbtuileslig == 3)
                        nvpoints = 4;

                    if (nbtuileslig == 4)
                        nvpoints = 5;

                    if (nbtuileslig == 5)
                        nvpoints = 12; // si il y a 6 tuiles dans la meme ligne alors il y a un bonus de 6 points(cette ligne est aussi appelee un qwirkle)

                }

                else if (nbtuilescol == 1)
                {
                    if (nbtuileslig == 1)
                        nvpoints = 3;

                    if (nbtuileslig == 2)
                        nvpoints = 4;

                    if (nbtuileslig == 3)
                        nvpoints = 5;

                    if (nbtuileslig == 4)
                        nvpoints = 6;

                    if (nbtuileslig == 5)
                        nvpoints = 13;
                }

                else if (nbtuilescol == 2)
                {
                    if (nbtuileslig == 1)
                        nvpoints = 4;

                    if (nbtuileslig == 2)
                        nvpoints = 5;

                    if (nbtuileslig == 3)
                        nvpoints = 6;

                    if (nbtuileslig == 4)
                        nvpoints = 7;

                    if (nbtuileslig == 5)
                        nvpoints = 14;
                }

                else if (nbtuilescol == 3)
                {
                    if (nbtuileslig == 1)
                        nvpoints = 5;

                    if (nbtuileslig == 2)
                        nvpoints = 6;

                    if (nbtuileslig == 3)
                        nvpoints = 7;

                    if (nbtuileslig == 4)
                        nvpoints = 8;

                    if (nbtuileslig == 5)
                        nvpoints = 15;
                }

                else if (nbtuilescol == 4)
                {
                    if (nbtuileslig == 1)
                        nvpoints = 6;

                    if (nbtuileslig == 2)
                        nvpoints = 7;

                    if (nbtuileslig == 3)
                        nvpoints = 8;

                    if (nbtuileslig == 4)
                        nvpoints = 9;

                    if (nbtuileslig == 5)
                        nvpoints = 16;
                }

                else if (nbtuilescol == 5) // puisqu'il y a 6 tuiles dans la colonne on ajoute 6 points (meme regle que pour les lignes)
                {
                    if (nbtuileslig == 1)
                        nvpoints = 13;

                    if (nbtuileslig == 2)
                        nvpoints = 14;

                    if (nbtuileslig == 3)
                        nvpoints = 15;

                    if (nbtuileslig == 4)
                        nvpoints = 16;

                    if (nbtuileslig == 5)
                        nvpoints = 23;
                }

            }
            nbtuiles += 1;
            score += nvpoints;
            return score;
        }

    }
}

