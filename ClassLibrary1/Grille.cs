﻿namespace ClassLibrary1
{
    public class Grille

    {
        internal const int taillemax = 30;
        internal int Nbcol;
        internal int Nblig;
        internal int nbtuiles;

        public Grille(int Nbcol, int Nblig, int nbliggrille, int nbcolgrille)
        {
            this.Nbcol = Nbcol;
            this.Nblig = Nblig;
            int[,] grille = new int[nbliggrille, nbcolgrille];
            nbtuiles = 0;
        }

        public Grille AggrandissementGrille(int nbliggrille, int nbcolgrille, int Nbcol, int Nblig, Grille grille) // gestion dynamique de la taille de la grille
        {
            int col, lig;
            while ((Nbcol <= taillemax) && (Nblig <= taillemax)) // Nblig et Nbcol sont les lignes et les colonnes ou sont placees des tuiles 
            {                                                    // tandis que Nbcolgrille et Nbliggrille designent la taille de la grille
                if (Nbcol >= 10 || Nblig >= 10)
                {
                    if (Nbcol >= 10 && Nblig >= 10)
                    {
                        for (col = 1; col <= Nbcol; col++)
                        {
                            nbcolgrille += 1;  // la grille s'aggrandit d'une colonne lorsqu'il y en a deja 10 ou plus
                        }

                        for (lig = 1; lig <= Nblig; lig++)
                        {
                            nbliggrille += 1; // la grille s'aggrandit d'une ligne lorsqu'il y en a deja 10 ou plus
                        }

                    }
                    else
                    {
                        if (Nbcol >= 10)
                        {
                            for (col = 10; col <= Nbcol; col++)
                            {
                                nbcolgrille += 1; // la grille s'aggrandit d'une colonne lorsqu'il y en a deja 10 ou plus
                            }
                        }

                        if (Nblig >= 10)
                        {
                            for (lig = 1; lig <= Nblig; lig++)
                            {
                                nbliggrille += 1; // la grille s'aggrandit d'une ligne lorsqu'il y en a deja 10 ou plus
                            }
                        }
                    }
                }
            }
            return grille;
        }
    }
}