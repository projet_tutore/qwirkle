﻿Imports ClassLibrary1
Public Class FormOption
    Private Sub BtnRetour_Click(sender As Object, e As EventArgs) Handles btnRetour.Click
        Me.Close()
    End Sub

    Private Sub BtnAjout_Click(sender As Object, e As EventArgs) Handles btnAjout.Click
        'Ajouter la classe d'ajouts de joueurs
        If lblJ3.Enabled = False Then
            lblJ3.Enabled = True
            lblDateJ3.Enabled = True
            NumJourNaisJ3.Enabled = True
            NumMoisNaisJ3.Enabled = True
            NumAnneeNaisJ3.Enabled = True
            TextBoxJ3.Enabled = True
            lblJourJ3.Enabled = True
            lblMoisJ3.Enabled = True
            lblAnneeJ3.Enabled = True

        ElseIf lblJ4.Enabled = False Then
            lblJ4.Enabled = True
            lblDateJ4.Enabled = True
            NumJourNaisJ4.Enabled = True
            NumMoisNaisJ4.Enabled = True
            NumAnneeNaisJ4.Enabled = True
            TextBoxJ4.Enabled = True
            lblJourJ4.Enabled = True
            lblMoisJ4.Enabled = True
            lblAnneeJ4.Enabled = True
        End If

        If lblNb.Text < 4 Then
            lblNb.Text = lblNb.Text + 1
        End If
        'Le nombre maximal du compteur est 4 
    End Sub

    Private Sub BtnSuppr_Click(sender As Object, e As EventArgs) Handles btnSuppr.Click
        'Ajouter la classe de supression de joueurs
        If lblJ4.Enabled = True Then
            lblJ4.Enabled = False
            lblDateJ4.Enabled = False
            NumJourNaisJ4.Enabled = False
            NumMoisNaisJ4.Enabled = False
            NumAnneeNaisJ4.Enabled = False
            TextBoxJ4.Enabled = False
            lblJourJ4.Enabled = False
            lblMoisJ4.Enabled = False
            lblAnneeJ4.Enabled = False

        ElseIf lblJ4.Enabled = False And lblJ3.Enabled = True Then
            lblJ3.Enabled = False
            lblDateJ3.Enabled = False
            NumJourNaisJ3.Enabled = False
            NumMoisNaisJ3.Enabled = False
            NumAnneeNaisJ3.Enabled = False
            TextBoxJ3.Enabled = False
            lblJourJ3.Enabled = False
            lblMoisJ3.Enabled = False
            lblAnneeJ3.Enabled = False
        End If

        If lblNb.Text > 2 Then
            lblNb.Text = lblNb.Text - 1
        End If
        'Le nombre minimum du compteur est 2
    End Sub

    Private Sub BtnStart_Click(sender As Object, e As EventArgs) Handles btnStart.Click

        If lblJ4.Enabled = True Then
            If TextBoxJ1.Text = "" Then
                MessageBox.Show("Veuillez remplir le Joueur 1.")
            ElseIf TextBoxJ2.Text = "" Then
                MessageBox.Show("Veuillez remplir le Joueur 2.")
            ElseIf TextBoxJ3.Text = "" Then
                MessageBox.Show("Veuillez remplir le Joueur 3.")
            ElseIf TextBoxJ4.Text = "" Then
                MessageBox.Show("Veuillez remplir le Joueur 4.")

            ElseIf TextBoxJ1.Text = TextBoxJ2.Text Or TextBoxJ1.Text = TextBoxJ3.Text Or TextBoxJ1.Text = TextBoxJ4.Text Or TextBoxJ2.Text = TextBoxJ3.Text Or TextBoxJ2.Text = TextBoxJ4.Text Or TextBoxJ3.Text = TextBoxJ4.Text Then
                MessageBox.Show("Les champs de joueurs ne peuvent être identiques !")

            Else
                Dim form As New FormJeu
                form.ShowDialog()
            End If
        End If

        If lblJ4.Enabled = False And lblJ3.Enabled = True Then
            If TextBoxJ1.Text = "" Then
                MessageBox.Show("Veuillez remplir le Joueur 1.")
            ElseIf TextBoxJ2.Text = "" Then
                MessageBox.Show("Veuillez remplir le Joueur 2.")
            ElseIf TextBoxJ3.Text = "" Then
                MessageBox.Show("Veuillez remplir le Joueur 3.")

            ElseIf TextBoxJ1.Text = TextBoxJ2.Text Or TextBoxJ1.Text = TextBoxJ3.Text Or TextBoxJ2.Text = TextBoxJ3.Text Then
                MessageBox.Show("Les champs de joueurs ne peuvent être identiques !")

            Else
                Dim form As New FormJeu
                form.ShowDialog()
            End If
        End If

        If lblJ3.Enabled = False Then
            If TextBoxJ1.Text = "" Then
                MessageBox.Show("Veuillez remplir le Joueur 1.")
            ElseIf TextBoxJ2.Text = "" Then
                MessageBox.Show("Veuillez remplir le Joueur 2.") 'On vérifie que les textbox sont remplis

            ElseIf TextBoxJ1.Text = TextBoxJ2.Text Then
                MessageBox.Show("Les champs de joueurs ne peuvent être identiques !")

            Else
                Dim form As New FormJeu
                form.ShowDialog()
            End If
            'A chaque fois qu'on appuyera sur le bouton start, on ouvrira un 
            'nouveau formulaire de façon à ne pas se retrouver sur la partie
            'précédente.
        End If
    End Sub
End Class
