﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormJeu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormJeu))
        Me.GrilleJ4 = New System.Windows.Forms.TableLayoutPanel()
        Me.tuileJ41 = New System.Windows.Forms.PictureBox()
        Me.tuileJ42 = New System.Windows.Forms.PictureBox()
        Me.tuileJ43 = New System.Windows.Forms.PictureBox()
        Me.tuileJ44 = New System.Windows.Forms.PictureBox()
        Me.tuileJ45 = New System.Windows.Forms.PictureBox()
        Me.tuileJ46 = New System.Windows.Forms.PictureBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.tuileJ21 = New System.Windows.Forms.PictureBox()
        Me.tuileJ22 = New System.Windows.Forms.PictureBox()
        Me.tuileJ23 = New System.Windows.Forms.PictureBox()
        Me.tuileJ24 = New System.Windows.Forms.PictureBox()
        Me.tuileJ25 = New System.Windows.Forms.PictureBox()
        Me.tuileJ26 = New System.Windows.Forms.PictureBox()
        Me.GrilleJ3 = New System.Windows.Forms.TableLayoutPanel()
        Me.tuileJ31 = New System.Windows.Forms.PictureBox()
        Me.tuileJ32 = New System.Windows.Forms.PictureBox()
        Me.tuileJ33 = New System.Windows.Forms.PictureBox()
        Me.tuileJ34 = New System.Windows.Forms.PictureBox()
        Me.tuileJ35 = New System.Windows.Forms.PictureBox()
        Me.tuileJ36 = New System.Windows.Forms.PictureBox()
        Me.mainJ1 = New System.Windows.Forms.TableLayoutPanel()
        Me.tuileJ15 = New System.Windows.Forms.PictureBox()
        Me.tuileJ13 = New System.Windows.Forms.PictureBox()
        Me.tuileJ14 = New System.Windows.Forms.PictureBox()
        Me.tuileJ11 = New System.Windows.Forms.PictureBox()
        Me.tuileJ12 = New System.Windows.Forms.PictureBox()
        Me.tuileJ16 = New System.Windows.Forms.PictureBox()
        Me.PointJ4 = New System.Windows.Forms.Label()
        Me.lblJ4Point = New System.Windows.Forms.Label()
        Me.lblJ4 = New System.Windows.Forms.Label()
        Me.PointJ3 = New System.Windows.Forms.Label()
        Me.lblJ3Point = New System.Windows.Forms.Label()
        Me.lblJ3 = New System.Windows.Forms.Label()
        Me.PointJ2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.PointJ1 = New System.Windows.Forms.Label()
        Me.lblPoint = New System.Windows.Forms.Label()
        Me.lblJ2 = New System.Windows.Forms.Label()
        Me.lblJ1 = New System.Windows.Forms.Label()
        Me.grille = New System.Windows.Forms.TableLayoutPanel()
        Me.btnInterdit = New System.Windows.Forms.Button()
        Me.btnMelange = New System.Windows.Forms.Button()
        Me.btnHome = New System.Windows.Forms.Button()
        Me.btnRetour = New System.Windows.Forms.Button()
        Me.btnValider = New System.Windows.Forms.Button()
        Me.GrilleJ4.SuspendLayout()
        CType(Me.tuileJ41, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ42, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ43, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ44, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ45, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ46, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.tuileJ21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ23, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ24, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ25, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ26, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GrilleJ3.SuspendLayout()
        CType(Me.tuileJ31, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ32, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ33, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ34, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ35, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ36, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mainJ1.SuspendLayout()
        CType(Me.tuileJ15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tuileJ16, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GrilleJ4
        '
        Me.GrilleJ4.Anchor = System.Windows.Forms.AnchorStyles.Top
        Me.GrilleJ4.ColumnCount = 6
        Me.GrilleJ4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.GrilleJ4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.GrilleJ4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.GrilleJ4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.GrilleJ4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.GrilleJ4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.GrilleJ4.Controls.Add(Me.tuileJ41, 0, 0)
        Me.GrilleJ4.Controls.Add(Me.tuileJ42, 1, 0)
        Me.GrilleJ4.Controls.Add(Me.tuileJ43, 2, 0)
        Me.GrilleJ4.Controls.Add(Me.tuileJ44, 3, 0)
        Me.GrilleJ4.Controls.Add(Me.tuileJ45, 4, 0)
        Me.GrilleJ4.Controls.Add(Me.tuileJ46, 5, 0)
        Me.GrilleJ4.Location = New System.Drawing.Point(217, 16)
        Me.GrilleJ4.Name = "GrilleJ4"
        Me.GrilleJ4.RowCount = 1
        Me.GrilleJ4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.GrilleJ4.Size = New System.Drawing.Size(350, 60)
        Me.GrilleJ4.TabIndex = 71
        '
        'tuileJ41
        '
        Me.tuileJ41.BackgroundImage = Global.[interface].My.Resources.Resources.CarreRouge
        Me.tuileJ41.Location = New System.Drawing.Point(3, 3)
        Me.tuileJ41.Name = "tuileJ41"
        Me.tuileJ41.Size = New System.Drawing.Size(52, 50)
        Me.tuileJ41.TabIndex = 0
        Me.tuileJ41.TabStop = False
        '
        'tuileJ42
        '
        Me.tuileJ42.BackgroundImage = Global.[interface].My.Resources.Resources.LosangeVert
        Me.tuileJ42.Location = New System.Drawing.Point(61, 3)
        Me.tuileJ42.Name = "tuileJ42"
        Me.tuileJ42.Size = New System.Drawing.Size(52, 50)
        Me.tuileJ42.TabIndex = 1
        Me.tuileJ42.TabStop = False
        '
        'tuileJ43
        '
        Me.tuileJ43.BackgroundImage = Global.[interface].My.Resources.Resources.TrefleViolet
        Me.tuileJ43.Location = New System.Drawing.Point(119, 3)
        Me.tuileJ43.Name = "tuileJ43"
        Me.tuileJ43.Size = New System.Drawing.Size(52, 50)
        Me.tuileJ43.TabIndex = 2
        Me.tuileJ43.TabStop = False
        '
        'tuileJ44
        '
        Me.tuileJ44.BackgroundImage = Global.[interface].My.Resources.Resources.TrefleOrange
        Me.tuileJ44.Location = New System.Drawing.Point(177, 3)
        Me.tuileJ44.Name = "tuileJ44"
        Me.tuileJ44.Size = New System.Drawing.Size(52, 50)
        Me.tuileJ44.TabIndex = 3
        Me.tuileJ44.TabStop = False
        '
        'tuileJ45
        '
        Me.tuileJ45.BackgroundImage = Global.[interface].My.Resources.Resources.CarreViolet
        Me.tuileJ45.Location = New System.Drawing.Point(235, 3)
        Me.tuileJ45.Name = "tuileJ45"
        Me.tuileJ45.Size = New System.Drawing.Size(52, 50)
        Me.tuileJ45.TabIndex = 4
        Me.tuileJ45.TabStop = False
        '
        'tuileJ46
        '
        Me.tuileJ46.BackgroundImage = Global.[interface].My.Resources.Resources.EtoileOrange
        Me.tuileJ46.Location = New System.Drawing.Point(293, 3)
        Me.tuileJ46.Name = "tuileJ46"
        Me.tuileJ46.Size = New System.Drawing.Size(54, 50)
        Me.tuileJ46.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.tuileJ46.TabIndex = 5
        Me.tuileJ46.TabStop = False
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.tuileJ21, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.tuileJ22, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.tuileJ23, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.tuileJ24, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.tuileJ25, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.tuileJ26, 0, 5)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(698, 77)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 6
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(60, 350)
        Me.TableLayoutPanel2.TabIndex = 69
        '
        'tuileJ21
        '
        Me.tuileJ21.BackgroundImage = Global.[interface].My.Resources.Resources.RondOrange
        Me.tuileJ21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tuileJ21.Location = New System.Drawing.Point(2, 2)
        Me.tuileJ21.Margin = New System.Windows.Forms.Padding(2)
        Me.tuileJ21.Name = "tuileJ21"
        Me.tuileJ21.Size = New System.Drawing.Size(56, 54)
        Me.tuileJ21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.tuileJ21.TabIndex = 0
        Me.tuileJ21.TabStop = False
        '
        'tuileJ22
        '
        Me.tuileJ22.BackgroundImage = Global.[interface].My.Resources.Resources.LosangeRouge
        Me.tuileJ22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tuileJ22.Location = New System.Drawing.Point(2, 60)
        Me.tuileJ22.Margin = New System.Windows.Forms.Padding(2)
        Me.tuileJ22.Name = "tuileJ22"
        Me.tuileJ22.Size = New System.Drawing.Size(56, 54)
        Me.tuileJ22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.tuileJ22.TabIndex = 1
        Me.tuileJ22.TabStop = False
        '
        'tuileJ23
        '
        Me.tuileJ23.BackgroundImage = Global.[interface].My.Resources.Resources.CarreJaune
        Me.tuileJ23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tuileJ23.Location = New System.Drawing.Point(2, 118)
        Me.tuileJ23.Margin = New System.Windows.Forms.Padding(2)
        Me.tuileJ23.Name = "tuileJ23"
        Me.tuileJ23.Size = New System.Drawing.Size(56, 54)
        Me.tuileJ23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.tuileJ23.TabIndex = 2
        Me.tuileJ23.TabStop = False
        '
        'tuileJ24
        '
        Me.tuileJ24.BackgroundImage = Global.[interface].My.Resources.Resources.CarreVert
        Me.tuileJ24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tuileJ24.Location = New System.Drawing.Point(2, 176)
        Me.tuileJ24.Margin = New System.Windows.Forms.Padding(2)
        Me.tuileJ24.Name = "tuileJ24"
        Me.tuileJ24.Size = New System.Drawing.Size(56, 54)
        Me.tuileJ24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.tuileJ24.TabIndex = 3
        Me.tuileJ24.TabStop = False
        '
        'tuileJ25
        '
        Me.tuileJ25.BackgroundImage = Global.[interface].My.Resources.Resources.LosangeOrange
        Me.tuileJ25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tuileJ25.Location = New System.Drawing.Point(2, 234)
        Me.tuileJ25.Margin = New System.Windows.Forms.Padding(2)
        Me.tuileJ25.Name = "tuileJ25"
        Me.tuileJ25.Size = New System.Drawing.Size(56, 54)
        Me.tuileJ25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.tuileJ25.TabIndex = 4
        Me.tuileJ25.TabStop = False
        '
        'tuileJ26
        '
        Me.tuileJ26.BackgroundImage = Global.[interface].My.Resources.Resources.RondBleu
        Me.tuileJ26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tuileJ26.Location = New System.Drawing.Point(2, 292)
        Me.tuileJ26.Margin = New System.Windows.Forms.Padding(2)
        Me.tuileJ26.Name = "tuileJ26"
        Me.tuileJ26.Size = New System.Drawing.Size(56, 56)
        Me.tuileJ26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.tuileJ26.TabIndex = 5
        Me.tuileJ26.TabStop = False
        '
        'GrilleJ3
        '
        Me.GrilleJ3.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.GrilleJ3.ColumnCount = 6
        Me.GrilleJ3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.GrilleJ3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.GrilleJ3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.GrilleJ3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.GrilleJ3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.GrilleJ3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.GrilleJ3.Controls.Add(Me.tuileJ31, 0, 0)
        Me.GrilleJ3.Controls.Add(Me.tuileJ32, 1, 0)
        Me.GrilleJ3.Controls.Add(Me.tuileJ33, 2, 0)
        Me.GrilleJ3.Controls.Add(Me.tuileJ34, 3, 0)
        Me.GrilleJ3.Controls.Add(Me.tuileJ35, 4, 0)
        Me.GrilleJ3.Controls.Add(Me.tuileJ36, 5, 0)
        Me.GrilleJ3.Location = New System.Drawing.Point(217, 463)
        Me.GrilleJ3.Name = "GrilleJ3"
        Me.GrilleJ3.RowCount = 1
        Me.GrilleJ3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.GrilleJ3.Size = New System.Drawing.Size(350, 60)
        Me.GrilleJ3.TabIndex = 70
        '
        'tuileJ31
        '
        Me.tuileJ31.BackgroundImage = Global.[interface].My.Resources.Resources.EtoileBleue
        Me.tuileJ31.Location = New System.Drawing.Point(3, 3)
        Me.tuileJ31.Name = "tuileJ31"
        Me.tuileJ31.Size = New System.Drawing.Size(52, 50)
        Me.tuileJ31.TabIndex = 0
        Me.tuileJ31.TabStop = False
        '
        'tuileJ32
        '
        Me.tuileJ32.BackgroundImage = Global.[interface].My.Resources.Resources.CroixOrange
        Me.tuileJ32.Location = New System.Drawing.Point(61, 3)
        Me.tuileJ32.Name = "tuileJ32"
        Me.tuileJ32.Size = New System.Drawing.Size(52, 50)
        Me.tuileJ32.TabIndex = 1
        Me.tuileJ32.TabStop = False
        '
        'tuileJ33
        '
        Me.tuileJ33.BackgroundImage = Global.[interface].My.Resources.Resources.CroixBleue
        Me.tuileJ33.Location = New System.Drawing.Point(119, 3)
        Me.tuileJ33.Name = "tuileJ33"
        Me.tuileJ33.Size = New System.Drawing.Size(52, 50)
        Me.tuileJ33.TabIndex = 2
        Me.tuileJ33.TabStop = False
        '
        'tuileJ34
        '
        Me.tuileJ34.BackgroundImage = Global.[interface].My.Resources.Resources.LosangeBleu
        Me.tuileJ34.Location = New System.Drawing.Point(177, 3)
        Me.tuileJ34.Name = "tuileJ34"
        Me.tuileJ34.Size = New System.Drawing.Size(52, 50)
        Me.tuileJ34.TabIndex = 3
        Me.tuileJ34.TabStop = False
        '
        'tuileJ35
        '
        Me.tuileJ35.BackgroundImage = Global.[interface].My.Resources.Resources.EtoileVerte
        Me.tuileJ35.Location = New System.Drawing.Point(235, 3)
        Me.tuileJ35.Name = "tuileJ35"
        Me.tuileJ35.Size = New System.Drawing.Size(52, 50)
        Me.tuileJ35.TabIndex = 4
        Me.tuileJ35.TabStop = False
        '
        'tuileJ36
        '
        Me.tuileJ36.BackgroundImage = Global.[interface].My.Resources.Resources.TrefleJaune
        Me.tuileJ36.Location = New System.Drawing.Point(293, 3)
        Me.tuileJ36.Name = "tuileJ36"
        Me.tuileJ36.Size = New System.Drawing.Size(54, 50)
        Me.tuileJ36.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.tuileJ36.TabIndex = 5
        Me.tuileJ36.TabStop = False
        '
        'mainJ1
        '
        Me.mainJ1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.mainJ1.ColumnCount = 1
        Me.mainJ1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.mainJ1.Controls.Add(Me.tuileJ15, 0, 4)
        Me.mainJ1.Controls.Add(Me.tuileJ13, 0, 2)
        Me.mainJ1.Controls.Add(Me.tuileJ14, 0, 3)
        Me.mainJ1.Controls.Add(Me.tuileJ11, 0, 0)
        Me.mainJ1.Controls.Add(Me.tuileJ12, 0, 1)
        Me.mainJ1.Controls.Add(Me.tuileJ16, 0, 5)
        Me.mainJ1.Location = New System.Drawing.Point(17, 77)
        Me.mainJ1.Name = "mainJ1"
        Me.mainJ1.RowCount = 6
        Me.mainJ1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.mainJ1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.mainJ1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.mainJ1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.mainJ1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.mainJ1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.mainJ1.Size = New System.Drawing.Size(60, 350)
        Me.mainJ1.TabIndex = 68
        '
        'tuileJ15
        '
        Me.tuileJ15.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.tuileJ15.BackgroundImage = Global.[interface].My.Resources.Resources.CarreBleu
        Me.tuileJ15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tuileJ15.Location = New System.Drawing.Point(2, 234)
        Me.tuileJ15.Margin = New System.Windows.Forms.Padding(2)
        Me.tuileJ15.Name = "tuileJ15"
        Me.tuileJ15.Size = New System.Drawing.Size(56, 54)
        Me.tuileJ15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.tuileJ15.TabIndex = 1
        Me.tuileJ15.TabStop = False
        '
        'tuileJ13
        '
        Me.tuileJ13.BackgroundImage = Global.[interface].My.Resources.Resources.LosangeJaune
        Me.tuileJ13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tuileJ13.Location = New System.Drawing.Point(2, 118)
        Me.tuileJ13.Margin = New System.Windows.Forms.Padding(2)
        Me.tuileJ13.Name = "tuileJ13"
        Me.tuileJ13.Size = New System.Drawing.Size(56, 54)
        Me.tuileJ13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.tuileJ13.TabIndex = 2
        Me.tuileJ13.TabStop = False
        '
        'tuileJ14
        '
        Me.tuileJ14.BackgroundImage = Global.[interface].My.Resources.Resources.TrefleBleu
        Me.tuileJ14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tuileJ14.Location = New System.Drawing.Point(2, 176)
        Me.tuileJ14.Margin = New System.Windows.Forms.Padding(2)
        Me.tuileJ14.Name = "tuileJ14"
        Me.tuileJ14.Size = New System.Drawing.Size(56, 54)
        Me.tuileJ14.TabIndex = 4
        Me.tuileJ14.TabStop = False
        '
        'tuileJ11
        '
        Me.tuileJ11.BackgroundImage = Global.[interface].My.Resources.Resources.CarreOrange
        Me.tuileJ11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tuileJ11.Location = New System.Drawing.Point(3, 3)
        Me.tuileJ11.Name = "tuileJ11"
        Me.tuileJ11.Size = New System.Drawing.Size(54, 52)
        Me.tuileJ11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.tuileJ11.TabIndex = 0
        Me.tuileJ11.TabStop = False
        '
        'tuileJ12
        '
        Me.tuileJ12.BackgroundImage = Global.[interface].My.Resources.Resources.RondViolet
        Me.tuileJ12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tuileJ12.Location = New System.Drawing.Point(2, 60)
        Me.tuileJ12.Margin = New System.Windows.Forms.Padding(2)
        Me.tuileJ12.Name = "tuileJ12"
        Me.tuileJ12.Size = New System.Drawing.Size(55, 54)
        Me.tuileJ12.TabIndex = 3
        Me.tuileJ12.TabStop = False
        '
        'tuileJ16
        '
        Me.tuileJ16.BackgroundImage = Global.[interface].My.Resources.Resources.RondRouge
        Me.tuileJ16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tuileJ16.Location = New System.Drawing.Point(2, 292)
        Me.tuileJ16.Margin = New System.Windows.Forms.Padding(2)
        Me.tuileJ16.Name = "tuileJ16"
        Me.tuileJ16.Size = New System.Drawing.Size(55, 53)
        Me.tuileJ16.TabIndex = 5
        Me.tuileJ16.TabStop = False
        '
        'PointJ4
        '
        Me.PointJ4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PointJ4.AutoSize = True
        Me.PointJ4.BackColor = System.Drawing.Color.Transparent
        Me.PointJ4.Location = New System.Drawing.Point(677, 42)
        Me.PointJ4.Name = "PointJ4"
        Me.PointJ4.Size = New System.Drawing.Size(13, 13)
        Me.PointJ4.TabIndex = 67
        Me.PointJ4.Text = "0"
        '
        'lblJ4Point
        '
        Me.lblJ4Point.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblJ4Point.AutoSize = True
        Me.lblJ4Point.BackColor = System.Drawing.Color.Transparent
        Me.lblJ4Point.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJ4Point.Location = New System.Drawing.Point(627, 42)
        Me.lblJ4Point.Name = "lblJ4Point"
        Me.lblJ4Point.Size = New System.Drawing.Size(54, 13)
        Me.lblJ4Point.TabIndex = 66
        Me.lblJ4Point.Text = "Points : "
        '
        'lblJ4
        '
        Me.lblJ4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblJ4.AutoSize = True
        Me.lblJ4.BackColor = System.Drawing.Color.Transparent
        Me.lblJ4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJ4.ForeColor = System.Drawing.Color.Red
        Me.lblJ4.Location = New System.Drawing.Point(622, 16)
        Me.lblJ4.Name = "lblJ4"
        Me.lblJ4.Size = New System.Drawing.Size(67, 16)
        Me.lblJ4.TabIndex = 65
        Me.lblJ4.Text = "Joueur 4"
        '
        'PointJ3
        '
        Me.PointJ3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.PointJ3.AutoSize = True
        Me.PointJ3.BackColor = System.Drawing.Color.Transparent
        Me.PointJ3.Location = New System.Drawing.Point(167, 510)
        Me.PointJ3.Name = "PointJ3"
        Me.PointJ3.Size = New System.Drawing.Size(13, 13)
        Me.PointJ3.TabIndex = 64
        Me.PointJ3.Text = "0"
        '
        'lblJ3Point
        '
        Me.lblJ3Point.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblJ3Point.AutoSize = True
        Me.lblJ3Point.BackColor = System.Drawing.Color.Transparent
        Me.lblJ3Point.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJ3Point.Location = New System.Drawing.Point(122, 510)
        Me.lblJ3Point.Name = "lblJ3Point"
        Me.lblJ3Point.Size = New System.Drawing.Size(54, 13)
        Me.lblJ3Point.TabIndex = 63
        Me.lblJ3Point.Text = "Points : "
        '
        'lblJ3
        '
        Me.lblJ3.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblJ3.AutoSize = True
        Me.lblJ3.BackColor = System.Drawing.Color.Transparent
        Me.lblJ3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJ3.ForeColor = System.Drawing.Color.Red
        Me.lblJ3.Location = New System.Drawing.Point(117, 484)
        Me.lblJ3.Name = "lblJ3"
        Me.lblJ3.Size = New System.Drawing.Size(71, 16)
        Me.lblJ3.TabIndex = 62
        Me.lblJ3.Text = "Joueur 3 "
        '
        'PointJ2
        '
        Me.PointJ2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PointJ2.AutoSize = True
        Me.PointJ2.BackColor = System.Drawing.Color.Transparent
        Me.PointJ2.Location = New System.Drawing.Point(743, 473)
        Me.PointJ2.Name = "PointJ2"
        Me.PointJ2.Size = New System.Drawing.Size(13, 13)
        Me.PointJ2.TabIndex = 56
        Me.PointJ2.Text = "0"
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(692, 473)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 55
        Me.Label3.Text = "Points : "
        '
        'PointJ1
        '
        Me.PointJ1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.PointJ1.AutoSize = True
        Me.PointJ1.BackColor = System.Drawing.Color.Transparent
        Me.PointJ1.Location = New System.Drawing.Point(72, 42)
        Me.PointJ1.Name = "PointJ1"
        Me.PointJ1.Size = New System.Drawing.Size(13, 13)
        Me.PointJ1.TabIndex = 54
        Me.PointJ1.Text = "0"
        '
        'lblPoint
        '
        Me.lblPoint.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblPoint.AutoSize = True
        Me.lblPoint.BackColor = System.Drawing.Color.Transparent
        Me.lblPoint.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPoint.Location = New System.Drawing.Point(26, 42)
        Me.lblPoint.Name = "lblPoint"
        Me.lblPoint.Size = New System.Drawing.Size(54, 13)
        Me.lblPoint.TabIndex = 53
        Me.lblPoint.Text = "Points : "
        '
        'lblJ2
        '
        Me.lblJ2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblJ2.AutoSize = True
        Me.lblJ2.BackColor = System.Drawing.Color.Transparent
        Me.lblJ2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJ2.ForeColor = System.Drawing.Color.Red
        Me.lblJ2.Location = New System.Drawing.Point(703, 447)
        Me.lblJ2.Name = "lblJ2"
        Me.lblJ2.Size = New System.Drawing.Size(67, 16)
        Me.lblJ2.TabIndex = 52
        Me.lblJ2.Text = "Joueur 2"
        '
        'lblJ1
        '
        Me.lblJ1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lblJ1.AutoSize = True
        Me.lblJ1.BackColor = System.Drawing.Color.Transparent
        Me.lblJ1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJ1.ForeColor = System.Drawing.Color.Red
        Me.lblJ1.Location = New System.Drawing.Point(14, 16)
        Me.lblJ1.Name = "lblJ1"
        Me.lblJ1.Size = New System.Drawing.Size(71, 16)
        Me.lblJ1.TabIndex = 51
        Me.lblJ1.Text = "Joueur 1 "
        '
        'grille
        '
        Me.grille.AllowDrop = True
        Me.grille.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.grille.ColumnCount = 11
        Me.grille.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.276525!))
        Me.grille.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.276525!))
        Me.grille.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.276525!))
        Me.grille.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.276525!))
        Me.grille.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.276525!))
        Me.grille.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.276525!))
        Me.grille.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.276525!))
        Me.grille.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.276525!))
        Me.grille.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.276525!))
        Me.grille.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.276525!))
        Me.grille.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.234762!))
        Me.grille.Location = New System.Drawing.Point(186, 92)
        Me.grille.Name = "grille"
        Me.grille.RowCount = 11
        Me.grille.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.276523!))
        Me.grille.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.276523!))
        Me.grille.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.276523!))
        Me.grille.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.276523!))
        Me.grille.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.276523!))
        Me.grille.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.276523!))
        Me.grille.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.276523!))
        Me.grille.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.276523!))
        Me.grille.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.276523!))
        Me.grille.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.276523!))
        Me.grille.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.23476!))
        Me.grille.Size = New System.Drawing.Size(415, 352)
        Me.grille.TabIndex = 50
        '
        'btnInterdit
        '
        Me.btnInterdit.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnInterdit.BackgroundImage = Global.[interface].My.Resources.Resources.imgInterdit
        Me.btnInterdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnInterdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnInterdit.Location = New System.Drawing.Point(522, 538)
        Me.btnInterdit.Name = "btnInterdit"
        Me.btnInterdit.Size = New System.Drawing.Size(55, 49)
        Me.btnInterdit.TabIndex = 61
        Me.btnInterdit.UseVisualStyleBackColor = True
        '
        'btnMelange
        '
        Me.btnMelange.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnMelange.BackgroundImage = Global.[interface].My.Resources.Resources.imgMelange3
        Me.btnMelange.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnMelange.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnMelange.Location = New System.Drawing.Point(468, 538)
        Me.btnMelange.Name = "btnMelange"
        Me.btnMelange.Size = New System.Drawing.Size(48, 49)
        Me.btnMelange.TabIndex = 60
        Me.btnMelange.UseVisualStyleBackColor = True
        '
        'btnHome
        '
        Me.btnHome.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnHome.BackgroundImage = Global.[interface].My.Resources.Resources.imgHome
        Me.btnHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnHome.Location = New System.Drawing.Point(365, 529)
        Me.btnHome.Name = "btnHome"
        Me.btnHome.Size = New System.Drawing.Size(81, 74)
        Me.btnHome.TabIndex = 59
        Me.btnHome.UseVisualStyleBackColor = True
        '
        'btnRetour
        '
        Me.btnRetour.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnRetour.BackgroundImage = Global.[interface].My.Resources.Resources.imgRetour
        Me.btnRetour.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnRetour.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnRetour.Location = New System.Drawing.Point(293, 538)
        Me.btnRetour.Name = "btnRetour"
        Me.btnRetour.Size = New System.Drawing.Size(54, 55)
        Me.btnRetour.TabIndex = 58
        Me.btnRetour.UseVisualStyleBackColor = True
        '
        'btnValider
        '
        Me.btnValider.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnValider.BackgroundImage = CType(resources.GetObject("btnValider.BackgroundImage"), System.Drawing.Image)
        Me.btnValider.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnValider.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnValider.Location = New System.Drawing.Point(217, 538)
        Me.btnValider.Name = "btnValider"
        Me.btnValider.Size = New System.Drawing.Size(59, 55)
        Me.btnValider.TabIndex = 57
        Me.btnValider.UseVisualStyleBackColor = True
        '
        'FormJeu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 609)
        Me.Controls.Add(Me.GrilleJ4)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Controls.Add(Me.GrilleJ3)
        Me.Controls.Add(Me.mainJ1)
        Me.Controls.Add(Me.PointJ4)
        Me.Controls.Add(Me.lblJ4Point)
        Me.Controls.Add(Me.lblJ4)
        Me.Controls.Add(Me.PointJ3)
        Me.Controls.Add(Me.lblJ3Point)
        Me.Controls.Add(Me.lblJ3)
        Me.Controls.Add(Me.btnInterdit)
        Me.Controls.Add(Me.btnMelange)
        Me.Controls.Add(Me.btnHome)
        Me.Controls.Add(Me.btnRetour)
        Me.Controls.Add(Me.btnValider)
        Me.Controls.Add(Me.PointJ2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.PointJ1)
        Me.Controls.Add(Me.lblPoint)
        Me.Controls.Add(Me.lblJ2)
        Me.Controls.Add(Me.lblJ1)
        Me.Controls.Add(Me.grille)
        Me.Name = "FormJeu"
        Me.Text = "FormJeu"
        Me.GrilleJ4.ResumeLayout(False)
        CType(Me.tuileJ41, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ42, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ43, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ44, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ45, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ46, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        CType(Me.tuileJ21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ23, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ24, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ25, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ26, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GrilleJ3.ResumeLayout(False)
        CType(Me.tuileJ31, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ32, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ33, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ34, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ35, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ36, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mainJ1.ResumeLayout(False)
        CType(Me.tuileJ15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tuileJ16, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GrilleJ4 As TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents tuileJ11 As PictureBox
    Friend WithEvents GrilleJ3 As TableLayoutPanel
    Friend WithEvents mainJ1 As TableLayoutPanel
    Friend WithEvents PointJ4 As Label
    Friend WithEvents lblJ4Point As Label
    Friend WithEvents lblJ4 As Label
    Friend WithEvents PointJ3 As Label
    Friend WithEvents lblJ3Point As Label
    Friend WithEvents lblJ3 As Label
    Friend WithEvents btnInterdit As Button
    Friend WithEvents btnMelange As Button
    Friend WithEvents btnHome As Button
    Friend WithEvents btnRetour As Button
    Friend WithEvents btnValider As Button
    Friend WithEvents PointJ2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents PointJ1 As Label
    Friend WithEvents lblPoint As Label
    Friend WithEvents lblJ2 As Label
    Friend WithEvents lblJ1 As Label
    Friend WithEvents grille As TableLayoutPanel
    Friend WithEvents tuileJ21 As PictureBox
    Friend WithEvents tuileJ22 As PictureBox
    Friend WithEvents tuileJ23 As PictureBox
    Friend WithEvents tuileJ24 As PictureBox
    Friend WithEvents tuileJ25 As PictureBox
    Friend WithEvents tuileJ26 As PictureBox
    Friend WithEvents tuileJ15 As PictureBox
    Friend WithEvents tuileJ13 As PictureBox
    Friend WithEvents tuileJ12 As PictureBox
    Friend WithEvents tuileJ14 As PictureBox
    Friend WithEvents tuileJ16 As PictureBox
    Friend WithEvents tuileJ41 As PictureBox
    Friend WithEvents tuileJ42 As PictureBox
    Friend WithEvents tuileJ43 As PictureBox
    Friend WithEvents tuileJ44 As PictureBox
    Friend WithEvents tuileJ45 As PictureBox
    Friend WithEvents tuileJ46 As PictureBox
    Friend WithEvents tuileJ31 As PictureBox
    Friend WithEvents tuileJ32 As PictureBox
    Friend WithEvents tuileJ33 As PictureBox
    Friend WithEvents tuileJ34 As PictureBox
    Friend WithEvents tuileJ35 As PictureBox
    Friend WithEvents tuileJ36 As PictureBox
End Class
