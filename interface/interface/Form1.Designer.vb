﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formQwirkle
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.imgQwirkle = New System.Windows.Forms.PictureBox()
        Me.btnJouer = New System.Windows.Forms.Button()
        Me.btnCommentJouer = New System.Windows.Forms.Button()
        Me.btnQuitter = New System.Windows.Forms.Button()
        CType(Me.imgQwirkle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'imgQwirkle
        '
        Me.imgQwirkle.Image = Global.[interface].My.Resources.Resources.logoQwirkle
        Me.imgQwirkle.Location = New System.Drawing.Point(187, 12)
        Me.imgQwirkle.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.imgQwirkle.Name = "imgQwirkle"
        Me.imgQwirkle.Size = New System.Drawing.Size(327, 105)
        Me.imgQwirkle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgQwirkle.TabIndex = 0
        Me.imgQwirkle.TabStop = False
        '
        'btnJouer
        '
        Me.btnJouer.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnJouer.Font = New System.Drawing.Font("Calibri", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnJouer.Location = New System.Drawing.Point(253, 160)
        Me.btnJouer.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnJouer.Name = "btnJouer"
        Me.btnJouer.Size = New System.Drawing.Size(213, 74)
        Me.btnJouer.TabIndex = 1
        Me.btnJouer.Text = "JOUER"
        Me.btnJouer.UseVisualStyleBackColor = False
        '
        'btnCommentJouer
        '
        Me.btnCommentJouer.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.btnCommentJouer.Location = New System.Drawing.Point(273, 258)
        Me.btnCommentJouer.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnCommentJouer.Name = "btnCommentJouer"
        Me.btnCommentJouer.Size = New System.Drawing.Size(173, 37)
        Me.btnCommentJouer.TabIndex = 2
        Me.btnCommentJouer.Text = "Comment jouer ?"
        Me.btnCommentJouer.UseVisualStyleBackColor = False
        '
        'btnQuitter
        '
        Me.btnQuitter.BackColor = System.Drawing.Color.Red
        Me.btnQuitter.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnQuitter.ForeColor = System.Drawing.Color.White
        Me.btnQuitter.Location = New System.Drawing.Point(293, 351)
        Me.btnQuitter.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnQuitter.Name = "btnQuitter"
        Me.btnQuitter.Size = New System.Drawing.Size(133, 43)
        Me.btnQuitter.TabIndex = 3
        Me.btnQuitter.Text = "Quitter"
        Me.btnQuitter.UseVisualStyleBackColor = False
        '
        'formQwirkle
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(699, 444)
        Me.Controls.Add(Me.btnQuitter)
        Me.Controls.Add(Me.btnCommentJouer)
        Me.Controls.Add(Me.btnJouer)
        Me.Controls.Add(Me.imgQwirkle)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "formQwirkle"
        Me.Text = "Qwirkle"
        CType(Me.imgQwirkle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents imgQwirkle As PictureBox
    Friend WithEvents btnJouer As Button
    Friend WithEvents btnCommentJouer As Button
    Friend WithEvents btnQuitter As Button
End Class
