﻿Imports QwirkleClass
Public Class FormJeu

    Private Sub BtnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Me.Close()
        FormOption.Close()
        'On ferme le jeu pour revenir à l'acceuil
    End Sub

    Private Sub LblJ3_VisibleChanged(sender As Object, e As EventArgs) Handles lblJ3.VisibleChanged, lblJ3Point.VisibleChanged, PointJ3.VisibleChanged, GrilleJ3.VisibleChanged
        If FormOption.lblJ3.Enabled = False Then
            lblJ3.Visible = False
            lblJ3Point.Visible = False
            PointJ3.Visible = False
            GrilleJ3.Visible = False
        End If
        'Si le joueur 3 n'est pas activé dans les options alors il ne s'affiche pas
    End Sub

    Private Sub LblJ4_VisibleChanged(sender As Object, e As EventArgs) Handles lblJ4.VisibleChanged, lblJ4Point.VisibleChanged, PointJ4.VisibleChanged, GrilleJ4.VisibleChanged
        If FormOption.lblJ4.Enabled = False Then
            lblJ4.Visible = False
            lblJ4Point.Visible = False
            PointJ4.Visible = False
            GrilleJ4.Visible = False
        End If
        'Si le joueur 4 n'est pas activé dans les options alors il ne s'affiche pas
    End Sub



    Private Sub PictureBox_MouseMove(sender As PictureBox, e As MouseEventArgs) Handles tuileJ11.MouseMove, tuileJ12.MouseMove, tuileJ13.MouseMove, tuileJ14.MouseMove, tuileJ15.MouseMove, tuileJ16.MouseMove, tuileJ21.MouseMove, tuileJ22.MouseMove, tuileJ23.MouseMove, tuileJ24.MouseMove, tuileJ25.MouseMove, tuileJ26.MouseMove, tuileJ31.MouseMove, tuileJ32.MouseMove, tuileJ33.MouseMove, tuileJ34.MouseMove, tuileJ35.MouseMove, tuileJ36.MouseMove, tuileJ41.MouseMove, tuileJ42.MouseMove, tuileJ43.MouseMove, tuileJ44.MouseMove, tuileJ45.MouseMove, tuileJ46.MouseMove
        Dim picD As PictureBox = sender
        Dim rep As DragDropEffects

        'Si on fait clique gauche sur l'image, on lance le dragdrop et on fait une copie de l'image
        If e.Button = MouseButtons.Left AndAlso picD IsNot Nothing Then
            rep = picD.DoDragDrop(picD.BackgroundImage, DragDropEffects.Move)
            If rep = DragDropEffects.Move Then
                sender.BackgroundImage = Nothing

            End If
            picD.AllowDrop = True
        End If
    End Sub

    Private Sub Grille_DragEnter(sender As Object, e As DragEventArgs)
        If e.Data.GetDataPresent(DataFormats.Bitmap) Then
            e.Effect = DragDropEffects.Move
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub Grille_DragDrop(sender As Object, e As DragEventArgs)
        Dim pic As PictureBox = sender
        pic.Image = e.Data.GetData(DataFormats.Bitmap)
    End Sub








    Private Sub BtnValider_Click(sender As Object, e As EventArgs) Handles btnValider.Click

        'Classe Valider son coup
    End Sub

    Private Sub BtnRetour_Click(sender As Object, e As EventArgs) Handles btnRetour.Click
        'Classe Retour
    End Sub

    Private Sub BtnMelange_Click(sender As Object, e As EventArgs) Handles btnMelange.Click
        'Classe changer de main
    End Sub

    Private Sub BtnInterdit_Click(sender As Object, e As EventArgs) Handles btnInterdit.Click
        'Classe Passer son tour
    End Sub

    Private Sub FormJeu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Classe Choisir Premier Joueur'
        'Classe Piocher'

        Dim ligne As Integer
        Dim colonne As Integer 'On créé un tableau pour faire la grille de 11
        For ligne = 0 To 10    'colonnes et 11 lignes
            For colonne = 0 To 10
                Dim pic As New PictureBox
                pic.AllowDrop = True    'On autorise le deplacement des images
                pic.BorderStyle = BorderStyle.Fixed3D
                'Dans chaque case nous mettons des bordures pour repérer les cases
                pic.SizeMode = PictureBoxSizeMode.StretchImage
                'On étire l'image pour remplir la case 
                grille.Controls.Add(pic, colonne, ligne)
                'On ajoute l'image dans la colonne et la ligne voulu
                AddHandler pic.DragEnter, AddressOf Grille_DragEnter
                AddHandler pic.DragDrop, AddressOf Grille_DragDrop
            Next
        Next
    End Sub

    Private Sub Label_VisibleChanged(sender As Object, e As EventArgs) Handles lblJ1.VisibleChanged, lblJ2.VisibleChanged, lblJ3.VisibleChanged, lblJ4.VisibleChanged
        lblJ1.Text = FormOption.TextBoxJ1.Text
        lblJ2.Text = FormOption.TextBoxJ2.Text
        lblJ3.Text = FormOption.TextBoxJ3.Text
        lblJ4.Text = FormOption.TextBoxJ4.Text
    End Sub
End Class

