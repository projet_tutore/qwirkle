﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormOption
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBoxJ4 = New System.Windows.Forms.GroupBox()
        Me.lblAnneeJ4 = New System.Windows.Forms.Label()
        Me.lblMoisJ4 = New System.Windows.Forms.Label()
        Me.NumAnneeNaisJ4 = New System.Windows.Forms.NumericUpDown()
        Me.NumMoisNaisJ4 = New System.Windows.Forms.NumericUpDown()
        Me.lblJourJ4 = New System.Windows.Forms.Label()
        Me.NumJourNaisJ4 = New System.Windows.Forms.NumericUpDown()
        Me.TextBoxJ4 = New System.Windows.Forms.TextBox()
        Me.lblDateJ4 = New System.Windows.Forms.Label()
        Me.lblJ4 = New System.Windows.Forms.Label()
        Me.GroupBoxJ3 = New System.Windows.Forms.GroupBox()
        Me.lblAnneeJ3 = New System.Windows.Forms.Label()
        Me.lblMoisJ3 = New System.Windows.Forms.Label()
        Me.NumAnneeNaisJ3 = New System.Windows.Forms.NumericUpDown()
        Me.NumMoisNaisJ3 = New System.Windows.Forms.NumericUpDown()
        Me.lblJourJ3 = New System.Windows.Forms.Label()
        Me.NumJourNaisJ3 = New System.Windows.Forms.NumericUpDown()
        Me.TextBoxJ3 = New System.Windows.Forms.TextBox()
        Me.lblDateJ3 = New System.Windows.Forms.Label()
        Me.lblJ3 = New System.Windows.Forms.Label()
        Me.GroupBoxJ2 = New System.Windows.Forms.GroupBox()
        Me.lblAnneeJ2 = New System.Windows.Forms.Label()
        Me.lblMoisJ2 = New System.Windows.Forms.Label()
        Me.NumAnneeNaisJ2 = New System.Windows.Forms.NumericUpDown()
        Me.NumMoisNaisJ2 = New System.Windows.Forms.NumericUpDown()
        Me.lblJourJ2 = New System.Windows.Forms.Label()
        Me.NumJourNaisJ2 = New System.Windows.Forms.NumericUpDown()
        Me.TextBoxJ2 = New System.Windows.Forms.TextBox()
        Me.lblDateJ2 = New System.Windows.Forms.Label()
        Me.lblJ2 = New System.Windows.Forms.Label()
        Me.GroupBoxJ1 = New System.Windows.Forms.GroupBox()
        Me.lblAnneeJ1 = New System.Windows.Forms.Label()
        Me.lblMoisJ1 = New System.Windows.Forms.Label()
        Me.NumAnneeNaisJ1 = New System.Windows.Forms.NumericUpDown()
        Me.NumMoisNaisJ1 = New System.Windows.Forms.NumericUpDown()
        Me.lblJourJ1 = New System.Windows.Forms.Label()
        Me.NumJourNaisJ1 = New System.Windows.Forms.NumericUpDown()
        Me.TextBoxJ1 = New System.Windows.Forms.TextBox()
        Me.lblDateJ1 = New System.Windows.Forms.Label()
        Me.lblJ1 = New System.Windows.Forms.Label()
        Me.btnStart = New System.Windows.Forms.Button()
        Me.btnSuppr = New System.Windows.Forms.Button()
        Me.btnAjout = New System.Windows.Forms.Button()
        Me.lblSuppr = New System.Windows.Forms.Label()
        Me.lblAjout = New System.Windows.Forms.Label()
        Me.lblNbMax = New System.Windows.Forms.Label()
        Me.lblNb = New System.Windows.Forms.Label()
        Me.btnRetour = New System.Windows.Forms.Button()
        Me.imgQwirkle = New System.Windows.Forms.PictureBox()
        Me.Panel1.SuspendLayout()
        Me.GroupBoxJ4.SuspendLayout()
        CType(Me.NumAnneeNaisJ4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumMoisNaisJ4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumJourNaisJ4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBoxJ3.SuspendLayout()
        CType(Me.NumAnneeNaisJ3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumMoisNaisJ3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumJourNaisJ3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBoxJ2.SuspendLayout()
        CType(Me.NumAnneeNaisJ2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumMoisNaisJ2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumJourNaisJ2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBoxJ1.SuspendLayout()
        CType(Me.NumAnneeNaisJ1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumMoisNaisJ1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.NumJourNaisJ1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imgQwirkle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.Controls.Add(Me.GroupBoxJ4)
        Me.Panel1.Controls.Add(Me.GroupBoxJ3)
        Me.Panel1.Controls.Add(Me.GroupBoxJ2)
        Me.Panel1.Controls.Add(Me.GroupBoxJ1)
        Me.Panel1.Location = New System.Drawing.Point(2, 101)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(539, 198)
        Me.Panel1.TabIndex = 2
        '
        'GroupBoxJ4
        '
        Me.GroupBoxJ4.Controls.Add(Me.lblAnneeJ4)
        Me.GroupBoxJ4.Controls.Add(Me.lblMoisJ4)
        Me.GroupBoxJ4.Controls.Add(Me.NumAnneeNaisJ4)
        Me.GroupBoxJ4.Controls.Add(Me.NumMoisNaisJ4)
        Me.GroupBoxJ4.Controls.Add(Me.lblJourJ4)
        Me.GroupBoxJ4.Controls.Add(Me.NumJourNaisJ4)
        Me.GroupBoxJ4.Controls.Add(Me.TextBoxJ4)
        Me.GroupBoxJ4.Controls.Add(Me.lblDateJ4)
        Me.GroupBoxJ4.Controls.Add(Me.lblJ4)
        Me.GroupBoxJ4.Location = New System.Drawing.Point(15, 231)
        Me.GroupBoxJ4.Name = "GroupBoxJ4"
        Me.GroupBoxJ4.Size = New System.Drawing.Size(491, 70)
        Me.GroupBoxJ4.TabIndex = 56
        Me.GroupBoxJ4.TabStop = False
        '
        'lblAnneeJ4
        '
        Me.lblAnneeJ4.AutoSize = True
        Me.lblAnneeJ4.Enabled = False
        Me.lblAnneeJ4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnneeJ4.Location = New System.Drawing.Point(400, 45)
        Me.lblAnneeJ4.Name = "lblAnneeJ4"
        Me.lblAnneeJ4.Size = New System.Drawing.Size(52, 16)
        Me.lblAnneeJ4.TabIndex = 53
        Me.lblAnneeJ4.Text = "Année"
        '
        'lblMoisJ4
        '
        Me.lblMoisJ4.AutoSize = True
        Me.lblMoisJ4.Enabled = False
        Me.lblMoisJ4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMoisJ4.Location = New System.Drawing.Point(305, 45)
        Me.lblMoisJ4.Name = "lblMoisJ4"
        Me.lblMoisJ4.Size = New System.Drawing.Size(20, 16)
        Me.lblMoisJ4.TabIndex = 52
        Me.lblMoisJ4.Text = "M"
        '
        'NumAnneeNaisJ4
        '
        Me.NumAnneeNaisJ4.Enabled = False
        Me.NumAnneeNaisJ4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumAnneeNaisJ4.Location = New System.Drawing.Point(347, 42)
        Me.NumAnneeNaisJ4.Maximum = New Decimal(New Integer() {2019, 0, 0, 0})
        Me.NumAnneeNaisJ4.Minimum = New Decimal(New Integer() {1950, 0, 0, 0})
        Me.NumAnneeNaisJ4.Name = "NumAnneeNaisJ4"
        Me.NumAnneeNaisJ4.Size = New System.Drawing.Size(50, 22)
        Me.NumAnneeNaisJ4.TabIndex = 51
        Me.NumAnneeNaisJ4.Value = New Decimal(New Integer() {1975, 0, 0, 0})
        '
        'NumMoisNaisJ4
        '
        Me.NumMoisNaisJ4.Enabled = False
        Me.NumMoisNaisJ4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumMoisNaisJ4.Location = New System.Drawing.Point(262, 42)
        Me.NumMoisNaisJ4.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.NumMoisNaisJ4.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumMoisNaisJ4.Name = "NumMoisNaisJ4"
        Me.NumMoisNaisJ4.Size = New System.Drawing.Size(41, 22)
        Me.NumMoisNaisJ4.TabIndex = 50
        Me.NumMoisNaisJ4.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblJourJ4
        '
        Me.lblJourJ4.AutoSize = True
        Me.lblJourJ4.Enabled = False
        Me.lblJourJ4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJourJ4.Location = New System.Drawing.Point(230, 45)
        Me.lblJourJ4.Name = "lblJourJ4"
        Me.lblJourJ4.Size = New System.Drawing.Size(16, 16)
        Me.lblJourJ4.TabIndex = 49
        Me.lblJourJ4.Text = "J"
        '
        'NumJourNaisJ4
        '
        Me.NumJourNaisJ4.Enabled = False
        Me.NumJourNaisJ4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumJourNaisJ4.Location = New System.Drawing.Point(187, 41)
        Me.NumJourNaisJ4.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.NumJourNaisJ4.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumJourNaisJ4.Name = "NumJourNaisJ4"
        Me.NumJourNaisJ4.Size = New System.Drawing.Size(41, 22)
        Me.NumJourNaisJ4.TabIndex = 48
        Me.NumJourNaisJ4.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'TextBoxJ4
        '
        Me.TextBoxJ4.Enabled = False
        Me.TextBoxJ4.Location = New System.Drawing.Point(190, 15)
        Me.TextBoxJ4.MaxLength = 12
        Me.TextBoxJ4.Name = "TextBoxJ4"
        Me.TextBoxJ4.Size = New System.Drawing.Size(207, 20)
        Me.TextBoxJ4.TabIndex = 2
        '
        'lblDateJ4
        '
        Me.lblDateJ4.AutoSize = True
        Me.lblDateJ4.Enabled = False
        Me.lblDateJ4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateJ4.Location = New System.Drawing.Point(6, 41)
        Me.lblDateJ4.Name = "lblDateJ4"
        Me.lblDateJ4.Size = New System.Drawing.Size(127, 16)
        Me.lblDateJ4.TabIndex = 1
        Me.lblDateJ4.Text = "Date de naissance :"
        '
        'lblJ4
        '
        Me.lblJ4.AutoSize = True
        Me.lblJ4.Enabled = False
        Me.lblJ4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJ4.ForeColor = System.Drawing.Color.Navy
        Me.lblJ4.Location = New System.Drawing.Point(6, 16)
        Me.lblJ4.Name = "lblJ4"
        Me.lblJ4.Size = New System.Drawing.Size(75, 16)
        Me.lblJ4.TabIndex = 0
        Me.lblJ4.Text = "Joueur 4 :"
        '
        'GroupBoxJ3
        '
        Me.GroupBoxJ3.Controls.Add(Me.lblAnneeJ3)
        Me.GroupBoxJ3.Controls.Add(Me.lblMoisJ3)
        Me.GroupBoxJ3.Controls.Add(Me.NumAnneeNaisJ3)
        Me.GroupBoxJ3.Controls.Add(Me.NumMoisNaisJ3)
        Me.GroupBoxJ3.Controls.Add(Me.lblJourJ3)
        Me.GroupBoxJ3.Controls.Add(Me.NumJourNaisJ3)
        Me.GroupBoxJ3.Controls.Add(Me.TextBoxJ3)
        Me.GroupBoxJ3.Controls.Add(Me.lblDateJ3)
        Me.GroupBoxJ3.Controls.Add(Me.lblJ3)
        Me.GroupBoxJ3.Location = New System.Drawing.Point(15, 155)
        Me.GroupBoxJ3.Name = "GroupBoxJ3"
        Me.GroupBoxJ3.Size = New System.Drawing.Size(491, 70)
        Me.GroupBoxJ3.TabIndex = 55
        Me.GroupBoxJ3.TabStop = False
        '
        'lblAnneeJ3
        '
        Me.lblAnneeJ3.AutoSize = True
        Me.lblAnneeJ3.Enabled = False
        Me.lblAnneeJ3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnneeJ3.Location = New System.Drawing.Point(400, 45)
        Me.lblAnneeJ3.Name = "lblAnneeJ3"
        Me.lblAnneeJ3.Size = New System.Drawing.Size(52, 16)
        Me.lblAnneeJ3.TabIndex = 53
        Me.lblAnneeJ3.Text = "Année"
        '
        'lblMoisJ3
        '
        Me.lblMoisJ3.AutoSize = True
        Me.lblMoisJ3.Enabled = False
        Me.lblMoisJ3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMoisJ3.Location = New System.Drawing.Point(305, 45)
        Me.lblMoisJ3.Name = "lblMoisJ3"
        Me.lblMoisJ3.Size = New System.Drawing.Size(20, 16)
        Me.lblMoisJ3.TabIndex = 52
        Me.lblMoisJ3.Text = "M"
        '
        'NumAnneeNaisJ3
        '
        Me.NumAnneeNaisJ3.Enabled = False
        Me.NumAnneeNaisJ3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumAnneeNaisJ3.Location = New System.Drawing.Point(347, 42)
        Me.NumAnneeNaisJ3.Maximum = New Decimal(New Integer() {2019, 0, 0, 0})
        Me.NumAnneeNaisJ3.Minimum = New Decimal(New Integer() {1950, 0, 0, 0})
        Me.NumAnneeNaisJ3.Name = "NumAnneeNaisJ3"
        Me.NumAnneeNaisJ3.Size = New System.Drawing.Size(50, 22)
        Me.NumAnneeNaisJ3.TabIndex = 51
        Me.NumAnneeNaisJ3.Value = New Decimal(New Integer() {1975, 0, 0, 0})
        '
        'NumMoisNaisJ3
        '
        Me.NumMoisNaisJ3.Enabled = False
        Me.NumMoisNaisJ3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumMoisNaisJ3.Location = New System.Drawing.Point(262, 42)
        Me.NumMoisNaisJ3.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.NumMoisNaisJ3.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumMoisNaisJ3.Name = "NumMoisNaisJ3"
        Me.NumMoisNaisJ3.Size = New System.Drawing.Size(41, 22)
        Me.NumMoisNaisJ3.TabIndex = 50
        Me.NumMoisNaisJ3.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblJourJ3
        '
        Me.lblJourJ3.AutoSize = True
        Me.lblJourJ3.Enabled = False
        Me.lblJourJ3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJourJ3.Location = New System.Drawing.Point(230, 45)
        Me.lblJourJ3.Name = "lblJourJ3"
        Me.lblJourJ3.Size = New System.Drawing.Size(16, 16)
        Me.lblJourJ3.TabIndex = 49
        Me.lblJourJ3.Text = "J"
        '
        'NumJourNaisJ3
        '
        Me.NumJourNaisJ3.Enabled = False
        Me.NumJourNaisJ3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumJourNaisJ3.Location = New System.Drawing.Point(187, 41)
        Me.NumJourNaisJ3.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.NumJourNaisJ3.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumJourNaisJ3.Name = "NumJourNaisJ3"
        Me.NumJourNaisJ3.Size = New System.Drawing.Size(41, 22)
        Me.NumJourNaisJ3.TabIndex = 48
        Me.NumJourNaisJ3.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'TextBoxJ3
        '
        Me.TextBoxJ3.Enabled = False
        Me.TextBoxJ3.Location = New System.Drawing.Point(190, 15)
        Me.TextBoxJ3.MaxLength = 12
        Me.TextBoxJ3.Name = "TextBoxJ3"
        Me.TextBoxJ3.Size = New System.Drawing.Size(207, 20)
        Me.TextBoxJ3.TabIndex = 2
        '
        'lblDateJ3
        '
        Me.lblDateJ3.AutoSize = True
        Me.lblDateJ3.Enabled = False
        Me.lblDateJ3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateJ3.Location = New System.Drawing.Point(6, 41)
        Me.lblDateJ3.Name = "lblDateJ3"
        Me.lblDateJ3.Size = New System.Drawing.Size(127, 16)
        Me.lblDateJ3.TabIndex = 1
        Me.lblDateJ3.Text = "Date de naissance :"
        '
        'lblJ3
        '
        Me.lblJ3.AutoSize = True
        Me.lblJ3.Enabled = False
        Me.lblJ3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJ3.ForeColor = System.Drawing.Color.Navy
        Me.lblJ3.Location = New System.Drawing.Point(6, 16)
        Me.lblJ3.Name = "lblJ3"
        Me.lblJ3.Size = New System.Drawing.Size(75, 16)
        Me.lblJ3.TabIndex = 0
        Me.lblJ3.Text = "Joueur 3 :"
        '
        'GroupBoxJ2
        '
        Me.GroupBoxJ2.Controls.Add(Me.lblAnneeJ2)
        Me.GroupBoxJ2.Controls.Add(Me.lblMoisJ2)
        Me.GroupBoxJ2.Controls.Add(Me.NumAnneeNaisJ2)
        Me.GroupBoxJ2.Controls.Add(Me.NumMoisNaisJ2)
        Me.GroupBoxJ2.Controls.Add(Me.lblJourJ2)
        Me.GroupBoxJ2.Controls.Add(Me.NumJourNaisJ2)
        Me.GroupBoxJ2.Controls.Add(Me.TextBoxJ2)
        Me.GroupBoxJ2.Controls.Add(Me.lblDateJ2)
        Me.GroupBoxJ2.Controls.Add(Me.lblJ2)
        Me.GroupBoxJ2.Location = New System.Drawing.Point(15, 79)
        Me.GroupBoxJ2.Name = "GroupBoxJ2"
        Me.GroupBoxJ2.Size = New System.Drawing.Size(491, 70)
        Me.GroupBoxJ2.TabIndex = 54
        Me.GroupBoxJ2.TabStop = False
        '
        'lblAnneeJ2
        '
        Me.lblAnneeJ2.AutoSize = True
        Me.lblAnneeJ2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnneeJ2.Location = New System.Drawing.Point(400, 45)
        Me.lblAnneeJ2.Name = "lblAnneeJ2"
        Me.lblAnneeJ2.Size = New System.Drawing.Size(52, 16)
        Me.lblAnneeJ2.TabIndex = 53
        Me.lblAnneeJ2.Text = "Année"
        '
        'lblMoisJ2
        '
        Me.lblMoisJ2.AutoSize = True
        Me.lblMoisJ2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMoisJ2.Location = New System.Drawing.Point(305, 45)
        Me.lblMoisJ2.Name = "lblMoisJ2"
        Me.lblMoisJ2.Size = New System.Drawing.Size(20, 16)
        Me.lblMoisJ2.TabIndex = 52
        Me.lblMoisJ2.Text = "M"
        '
        'NumAnneeNaisJ2
        '
        Me.NumAnneeNaisJ2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumAnneeNaisJ2.Location = New System.Drawing.Point(347, 42)
        Me.NumAnneeNaisJ2.Maximum = New Decimal(New Integer() {2019, 0, 0, 0})
        Me.NumAnneeNaisJ2.Minimum = New Decimal(New Integer() {1950, 0, 0, 0})
        Me.NumAnneeNaisJ2.Name = "NumAnneeNaisJ2"
        Me.NumAnneeNaisJ2.Size = New System.Drawing.Size(50, 22)
        Me.NumAnneeNaisJ2.TabIndex = 51
        Me.NumAnneeNaisJ2.Value = New Decimal(New Integer() {1975, 0, 0, 0})
        '
        'NumMoisNaisJ2
        '
        Me.NumMoisNaisJ2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumMoisNaisJ2.Location = New System.Drawing.Point(262, 42)
        Me.NumMoisNaisJ2.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.NumMoisNaisJ2.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumMoisNaisJ2.Name = "NumMoisNaisJ2"
        Me.NumMoisNaisJ2.Size = New System.Drawing.Size(41, 22)
        Me.NumMoisNaisJ2.TabIndex = 50
        Me.NumMoisNaisJ2.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblJourJ2
        '
        Me.lblJourJ2.AutoSize = True
        Me.lblJourJ2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJourJ2.Location = New System.Drawing.Point(230, 45)
        Me.lblJourJ2.Name = "lblJourJ2"
        Me.lblJourJ2.Size = New System.Drawing.Size(16, 16)
        Me.lblJourJ2.TabIndex = 49
        Me.lblJourJ2.Text = "J"
        '
        'NumJourNaisJ2
        '
        Me.NumJourNaisJ2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumJourNaisJ2.Location = New System.Drawing.Point(187, 41)
        Me.NumJourNaisJ2.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.NumJourNaisJ2.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumJourNaisJ2.Name = "NumJourNaisJ2"
        Me.NumJourNaisJ2.Size = New System.Drawing.Size(41, 22)
        Me.NumJourNaisJ2.TabIndex = 48
        Me.NumJourNaisJ2.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'TextBoxJ2
        '
        Me.TextBoxJ2.Location = New System.Drawing.Point(190, 15)
        Me.TextBoxJ2.MaxLength = 12
        Me.TextBoxJ2.Name = "TextBoxJ2"
        Me.TextBoxJ2.Size = New System.Drawing.Size(207, 20)
        Me.TextBoxJ2.TabIndex = 2
        '
        'lblDateJ2
        '
        Me.lblDateJ2.AutoSize = True
        Me.lblDateJ2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateJ2.Location = New System.Drawing.Point(6, 41)
        Me.lblDateJ2.Name = "lblDateJ2"
        Me.lblDateJ2.Size = New System.Drawing.Size(127, 16)
        Me.lblDateJ2.TabIndex = 1
        Me.lblDateJ2.Text = "Date de naissance :"
        '
        'lblJ2
        '
        Me.lblJ2.AutoSize = True
        Me.lblJ2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJ2.ForeColor = System.Drawing.Color.Navy
        Me.lblJ2.Location = New System.Drawing.Point(6, 16)
        Me.lblJ2.Name = "lblJ2"
        Me.lblJ2.Size = New System.Drawing.Size(75, 16)
        Me.lblJ2.TabIndex = 0
        Me.lblJ2.Text = "Joueur 2 :"
        '
        'GroupBoxJ1
        '
        Me.GroupBoxJ1.Controls.Add(Me.lblAnneeJ1)
        Me.GroupBoxJ1.Controls.Add(Me.lblMoisJ1)
        Me.GroupBoxJ1.Controls.Add(Me.NumAnneeNaisJ1)
        Me.GroupBoxJ1.Controls.Add(Me.NumMoisNaisJ1)
        Me.GroupBoxJ1.Controls.Add(Me.lblJourJ1)
        Me.GroupBoxJ1.Controls.Add(Me.NumJourNaisJ1)
        Me.GroupBoxJ1.Controls.Add(Me.TextBoxJ1)
        Me.GroupBoxJ1.Controls.Add(Me.lblDateJ1)
        Me.GroupBoxJ1.Controls.Add(Me.lblJ1)
        Me.GroupBoxJ1.Location = New System.Drawing.Point(15, 3)
        Me.GroupBoxJ1.Name = "GroupBoxJ1"
        Me.GroupBoxJ1.Size = New System.Drawing.Size(491, 70)
        Me.GroupBoxJ1.TabIndex = 0
        Me.GroupBoxJ1.TabStop = False
        '
        'lblAnneeJ1
        '
        Me.lblAnneeJ1.AutoSize = True
        Me.lblAnneeJ1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnneeJ1.Location = New System.Drawing.Point(400, 45)
        Me.lblAnneeJ1.Name = "lblAnneeJ1"
        Me.lblAnneeJ1.Size = New System.Drawing.Size(52, 16)
        Me.lblAnneeJ1.TabIndex = 53
        Me.lblAnneeJ1.Text = "Année"
        '
        'lblMoisJ1
        '
        Me.lblMoisJ1.AutoSize = True
        Me.lblMoisJ1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMoisJ1.Location = New System.Drawing.Point(305, 45)
        Me.lblMoisJ1.Name = "lblMoisJ1"
        Me.lblMoisJ1.Size = New System.Drawing.Size(20, 16)
        Me.lblMoisJ1.TabIndex = 52
        Me.lblMoisJ1.Text = "M"
        '
        'NumAnneeNaisJ1
        '
        Me.NumAnneeNaisJ1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumAnneeNaisJ1.Location = New System.Drawing.Point(347, 42)
        Me.NumAnneeNaisJ1.Maximum = New Decimal(New Integer() {2019, 0, 0, 0})
        Me.NumAnneeNaisJ1.Minimum = New Decimal(New Integer() {1950, 0, 0, 0})
        Me.NumAnneeNaisJ1.Name = "NumAnneeNaisJ1"
        Me.NumAnneeNaisJ1.Size = New System.Drawing.Size(50, 22)
        Me.NumAnneeNaisJ1.TabIndex = 51
        Me.NumAnneeNaisJ1.Value = New Decimal(New Integer() {1975, 0, 0, 0})
        '
        'NumMoisNaisJ1
        '
        Me.NumMoisNaisJ1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumMoisNaisJ1.Location = New System.Drawing.Point(262, 42)
        Me.NumMoisNaisJ1.Maximum = New Decimal(New Integer() {12, 0, 0, 0})
        Me.NumMoisNaisJ1.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumMoisNaisJ1.Name = "NumMoisNaisJ1"
        Me.NumMoisNaisJ1.Size = New System.Drawing.Size(41, 22)
        Me.NumMoisNaisJ1.TabIndex = 50
        Me.NumMoisNaisJ1.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblJourJ1
        '
        Me.lblJourJ1.AutoSize = True
        Me.lblJourJ1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJourJ1.Location = New System.Drawing.Point(230, 45)
        Me.lblJourJ1.Name = "lblJourJ1"
        Me.lblJourJ1.Size = New System.Drawing.Size(16, 16)
        Me.lblJourJ1.TabIndex = 49
        Me.lblJourJ1.Text = "J"
        '
        'NumJourNaisJ1
        '
        Me.NumJourNaisJ1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NumJourNaisJ1.Location = New System.Drawing.Point(187, 41)
        Me.NumJourNaisJ1.Maximum = New Decimal(New Integer() {31, 0, 0, 0})
        Me.NumJourNaisJ1.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.NumJourNaisJ1.Name = "NumJourNaisJ1"
        Me.NumJourNaisJ1.Size = New System.Drawing.Size(41, 22)
        Me.NumJourNaisJ1.TabIndex = 48
        Me.NumJourNaisJ1.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'TextBoxJ1
        '
        Me.TextBoxJ1.Location = New System.Drawing.Point(190, 15)
        Me.TextBoxJ1.MaxLength = 12
        Me.TextBoxJ1.Name = "TextBoxJ1"
        Me.TextBoxJ1.Size = New System.Drawing.Size(207, 20)
        Me.TextBoxJ1.TabIndex = 2
        '
        'lblDateJ1
        '
        Me.lblDateJ1.AutoSize = True
        Me.lblDateJ1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateJ1.Location = New System.Drawing.Point(6, 41)
        Me.lblDateJ1.Name = "lblDateJ1"
        Me.lblDateJ1.Size = New System.Drawing.Size(127, 16)
        Me.lblDateJ1.TabIndex = 1
        Me.lblDateJ1.Text = "Date de naissance :"
        '
        'lblJ1
        '
        Me.lblJ1.AutoSize = True
        Me.lblJ1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblJ1.ForeColor = System.Drawing.Color.Navy
        Me.lblJ1.Location = New System.Drawing.Point(6, 16)
        Me.lblJ1.Name = "lblJ1"
        Me.lblJ1.Size = New System.Drawing.Size(75, 16)
        Me.lblJ1.TabIndex = 0
        Me.lblJ1.Text = "Joueur 1 :"
        '
        'btnStart
        '
        Me.btnStart.BackColor = System.Drawing.Color.Yellow
        Me.btnStart.Font = New System.Drawing.Font("Calibri", 12.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStart.Location = New System.Drawing.Point(193, 306)
        Me.btnStart.Name = "btnStart"
        Me.btnStart.Size = New System.Drawing.Size(165, 35)
        Me.btnStart.TabIndex = 3
        Me.btnStart.Text = "Lancer la partie"
        Me.btnStart.UseVisualStyleBackColor = False
        '
        'btnSuppr
        '
        Me.btnSuppr.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnSuppr.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSuppr.Location = New System.Drawing.Point(12, 332)
        Me.btnSuppr.Name = "btnSuppr"
        Me.btnSuppr.Size = New System.Drawing.Size(23, 23)
        Me.btnSuppr.TabIndex = 15
        Me.btnSuppr.Text = "-"
        Me.btnSuppr.UseVisualStyleBackColor = True
        '
        'btnAjout
        '
        Me.btnAjout.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.btnAjout.Location = New System.Drawing.Point(12, 306)
        Me.btnAjout.Name = "btnAjout"
        Me.btnAjout.Size = New System.Drawing.Size(23, 23)
        Me.btnAjout.TabIndex = 14
        Me.btnAjout.Text = "+"
        Me.btnAjout.UseVisualStyleBackColor = True
        '
        'lblSuppr
        '
        Me.lblSuppr.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblSuppr.AutoSize = True
        Me.lblSuppr.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSuppr.Location = New System.Drawing.Point(47, 337)
        Me.lblSuppr.Name = "lblSuppr"
        Me.lblSuppr.Size = New System.Drawing.Size(120, 13)
        Me.lblSuppr.TabIndex = 13
        Me.lblSuppr.Text = "Supprimer un joueur"
        '
        'lblAjout
        '
        Me.lblAjout.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblAjout.AutoSize = True
        Me.lblAjout.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAjout.Location = New System.Drawing.Point(47, 311)
        Me.lblAjout.Name = "lblAjout"
        Me.lblAjout.Size = New System.Drawing.Size(104, 13)
        Me.lblAjout.TabIndex = 12
        Me.lblAjout.Text = "Ajouter un joueur"
        '
        'lblNbMax
        '
        Me.lblNbMax.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblNbMax.AutoSize = True
        Me.lblNbMax.Location = New System.Drawing.Point(477, 337)
        Me.lblNbMax.Name = "lblNbMax"
        Me.lblNbMax.Size = New System.Drawing.Size(55, 13)
        Me.lblNbMax.TabIndex = 17
        Me.lblNbMax.Text = "/4 joueurs"
        '
        'lblNb
        '
        Me.lblNb.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblNb.AutoSize = True
        Me.lblNb.Location = New System.Drawing.Point(468, 337)
        Me.lblNb.Name = "lblNb"
        Me.lblNb.Size = New System.Drawing.Size(13, 13)
        Me.lblNb.TabIndex = 16
        Me.lblNb.Text = "2"
        '
        'btnRetour
        '
        Me.btnRetour.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnRetour.Image = Global.[interface].My.Resources.Resources.btnRetour
        Me.btnRetour.Location = New System.Drawing.Point(17, 12)
        Me.btnRetour.Name = "btnRetour"
        Me.btnRetour.Size = New System.Drawing.Size(60, 30)
        Me.btnRetour.TabIndex = 18
        Me.btnRetour.UseVisualStyleBackColor = True
        '
        'imgQwirkle
        '
        Me.imgQwirkle.Image = Global.[interface].My.Resources.Resources.logoQwirkle
        Me.imgQwirkle.Location = New System.Drawing.Point(140, 10)
        Me.imgQwirkle.Name = "imgQwirkle"
        Me.imgQwirkle.Size = New System.Drawing.Size(245, 85)
        Me.imgQwirkle.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imgQwirkle.TabIndex = 1
        Me.imgQwirkle.TabStop = False
        '
        'FormOption
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(544, 361)
        Me.Controls.Add(Me.btnRetour)
        Me.Controls.Add(Me.lblNbMax)
        Me.Controls.Add(Me.lblNb)
        Me.Controls.Add(Me.btnSuppr)
        Me.Controls.Add(Me.btnAjout)
        Me.Controls.Add(Me.lblSuppr)
        Me.Controls.Add(Me.lblAjout)
        Me.Controls.Add(Me.btnStart)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.imgQwirkle)
        Me.Name = "FormOption"
        Me.Text = "FormOption"
        Me.Panel1.ResumeLayout(False)
        Me.GroupBoxJ4.ResumeLayout(False)
        Me.GroupBoxJ4.PerformLayout()
        CType(Me.NumAnneeNaisJ4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumMoisNaisJ4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumJourNaisJ4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBoxJ3.ResumeLayout(False)
        Me.GroupBoxJ3.PerformLayout()
        CType(Me.NumAnneeNaisJ3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumMoisNaisJ3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumJourNaisJ3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBoxJ2.ResumeLayout(False)
        Me.GroupBoxJ2.PerformLayout()
        CType(Me.NumAnneeNaisJ2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumMoisNaisJ2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumJourNaisJ2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBoxJ1.ResumeLayout(False)
        Me.GroupBoxJ1.PerformLayout()
        CType(Me.NumAnneeNaisJ1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumMoisNaisJ1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.NumJourNaisJ1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imgQwirkle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents imgQwirkle As PictureBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnStart As Button
    Friend WithEvents GroupBoxJ4 As GroupBox
    Friend WithEvents lblAnneeJ4 As Label
    Friend WithEvents lblMoisJ4 As Label
    Friend WithEvents NumAnneeNaisJ4 As NumericUpDown
    Friend WithEvents NumMoisNaisJ4 As NumericUpDown
    Friend WithEvents lblJourJ4 As Label
    Friend WithEvents NumJourNaisJ4 As NumericUpDown
    Friend WithEvents TextBoxJ4 As TextBox
    Friend WithEvents lblDateJ4 As Label
    Friend WithEvents lblJ4 As Label
    Friend WithEvents GroupBoxJ3 As GroupBox
    Friend WithEvents lblAnneeJ3 As Label
    Friend WithEvents lblMoisJ3 As Label
    Friend WithEvents NumAnneeNaisJ3 As NumericUpDown
    Friend WithEvents NumMoisNaisJ3 As NumericUpDown
    Friend WithEvents lblJourJ3 As Label
    Friend WithEvents NumJourNaisJ3 As NumericUpDown
    Friend WithEvents TextBoxJ3 As TextBox
    Friend WithEvents lblDateJ3 As Label
    Friend WithEvents lblJ3 As Label
    Friend WithEvents GroupBoxJ2 As GroupBox
    Friend WithEvents lblAnneeJ2 As Label
    Friend WithEvents lblMoisJ2 As Label
    Friend WithEvents NumAnneeNaisJ2 As NumericUpDown
    Friend WithEvents NumMoisNaisJ2 As NumericUpDown
    Friend WithEvents lblJourJ2 As Label
    Friend WithEvents NumJourNaisJ2 As NumericUpDown
    Friend WithEvents TextBoxJ2 As TextBox
    Friend WithEvents lblDateJ2 As Label
    Friend WithEvents lblJ2 As Label
    Friend WithEvents GroupBoxJ1 As GroupBox
    Friend WithEvents lblAnneeJ1 As Label
    Friend WithEvents lblMoisJ1 As Label
    Friend WithEvents NumAnneeNaisJ1 As NumericUpDown
    Friend WithEvents NumMoisNaisJ1 As NumericUpDown
    Friend WithEvents lblJourJ1 As Label
    Friend WithEvents NumJourNaisJ1 As NumericUpDown
    Friend WithEvents TextBoxJ1 As TextBox
    Friend WithEvents lblDateJ1 As Label
    Friend WithEvents lblJ1 As Label
    Friend WithEvents btnSuppr As Button
    Friend WithEvents btnAjout As Button
    Friend WithEvents lblSuppr As Label
    Friend WithEvents lblAjout As Label
    Friend WithEvents lblNbMax As Label
    Friend WithEvents lblNb As Label
    Friend WithEvents btnRetour As Button
End Class
